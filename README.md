# Floyd/Pulse Data Processing

### Deliveroo Payments Reports Merge

The _dlvr_payments_file_merge.py_ script merges multiple Deliveroo payments reports into a single, correctly formatted 
and named report.

Instructions:

1. Place the multiple payments report files into a directory with no other files
2. Modify the _dlvr_payments_merge_config.ini file_ with the path to the directory (the output directory can also be 
   specified).
3. Input the correct timeperiod in the _dlvr_payments_merge_config.ini_ file 
4. Run the script _dlvr_payments_file_merge.py_ script

**NB**: The format of all payments reports being merged must be identical. 

