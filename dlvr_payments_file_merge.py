"""
Date: 09-Aug-2021
Purpose:

The following script combines multiple Deliveroo payments files into one. It assumes all payment files follow the same
format and consist of 3 sections with the following titles in the order indicated:

1. Orders and related adjustments
2. Payments for contested customer refunds (from a previous invoice)
3. Other payments and fees

The script will loop over each input file, extract the 3 sections into separate lists and then combine the appropriate
lists together for different files under the correct title and section headings before writing out the merged data
to an output file.
"""

import csv
from configparser import ConfigParser
import os
import fnmatch

# Reading in config params
parser = ConfigParser()
parser.read('dlvr_payments_merge_config.ini')
input_dir = parser['vars'].get('input_directory')
output_dir = parser['vars'].get('output_directory')
timeperiod = parser['vars'].get('timeperiod')

# Retrieving list of csv files in the input directory
csvfiles = fnmatch.filter(os.listdir(input_dir), '*.csv')

sect2_title = "Payments for contested customer refunds (from a previous invoice)"
sect3_title = "Other payments and fees"

sect1_data = []
sect2_data = []
sect3_data = []

with open(f'{output_dir}payments_timeperiod-{timeperiod}.csv', 'w', newline='', encoding='UTF8') as file_out:
    f_writer = csv.writer(file_out, delimiter=',', quotechar='"')

    for fn, f in enumerate(csvfiles):
        all_data = []
        with open(input_dir+f, 'r', newline='', encoding='UTF8') as file_in:
            f_reader = csv.reader(file_in, delimiter=',', quotechar='"')

            for i, row in enumerate(f_reader):
                all_data.append(row)

                if sect2_title in row:
                    sect2_index = i
                elif sect3_title in row:
                    sect3_index = i

        # Adding the titles and column headers for each section to the output file
        if fn == 0:
            sect1_data = all_data[0:2]
            sect2_data = all_data[sect2_index:sect2_index + 2]
            sect3_data = all_data[sect3_index:sect3_index + 2]

        sect1_data = sect1_data + all_data[2:sect2_index-1]
        sect2_data = sect2_data + all_data[sect2_index+2:sect3_index-1]
        sect3_data = sect3_data + all_data[sect3_index+2:]

    f_writer.writerows(sect1_data+[[]]+sect2_data+[[]]+sect3_data)
